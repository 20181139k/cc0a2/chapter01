/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davis.alderete.v.p22050701;

/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Functions");
        Function();
        Function("Davis");
        Function("Davis",22);
        String[] result={"Hello","Davis",",","you","are","22"};
        Function(result);
        int sum=Function(1,2,3,4,5,6,7,8,9);
        System.out.println("sum: "+String.valueOf(sum));
        
        StringBuilder stringNumber=new StringBuilder();
        stringNumber.append("0");
        System.out.println("StringNumber: "+ stringNumber);
        Function(stringNumber);
        System.out.println("StringNumber: "+stringNumber);
        stringNumber.setLength(0);
        if(authentication("1",stringNumber)){
            System.out.println("Authentication is ok!!!");
        }else{
            System.out.println("Authentication failed");
        }
        
    }
    public static void Function(){
        System.out.println("Hello World");
    }
    public static void Function(String name){
        System.out.println("Hello "+name+" !!!");
    }
    public static void Function(String name,int age){
        System.out.println("hello "+name+" you are "+String.valueOf(age)+"years old !!!");
    }
    public static void Function(String[] args){
        for(String arg:args){
            System.out.print(arg);
        }
        System.out.println("");
    }
    
    //suma (int a , int b)
    
    /**
     * Add many numbers
     * @param numbers every number in the argument
     * @return sum of numbers
     */
    public static int Function(int... numbers){
        int sum=0;
        for(int number:numbers){
            sum+=number;
        }
        return sum;
    }
    public static void Function(StringBuilder reference){
        reference.setLength(0);
        reference.append("1");
    }
    public static boolean authentication(StringBuilder credential,StringBuilder error){
        boolean out=false;
        if(credential.equals("1")){
            out=true;
            error.setLength(0);
            error.append("0");
        }else{
            error.append("-1");
        }
        return out;
    }
}
