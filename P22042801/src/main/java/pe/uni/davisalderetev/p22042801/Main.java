/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davisalderetev.p22042801;

/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class Main {
    
    public static void main(String[] args){
        System.out.println("it is my first proyect: ");
        System.out.println("longitud del array: "+args.length);
        int a=Integer.parseInt(args[0]);
        int b=Integer.parseInt(args[1]);
        System.out.println("suma de dos numeros es: "+(a+b));
        System.out.println("operaciones unitarias: ");
        System.out.println("this is a incremented in 1: "+ (++a));
        
        System.out.println("Bitwyse operators");
        System.out.println(4&7);
        
        //expresamos 4 en binario: 00000100
        //expresamos 7 en binario: 00000111
        //usando la operacion & resulta lo siguiente en binario: 0000100: 4
        System.out.println("Logical operators: ");
        boolean b1=true;
        boolean b2=false;
        System.out.println(b1 && b2);
        System.out.println(b1 || b2);
        System.out.println(b1 && !b2);
        System.out.println(!b1 || b2);
        
        System.out.println("Asignament operators: ");
        
        int d=4;
        System.out.println(d);
        d+=8; //d=d+8
        System.out.println(d);
        d-=2;   //d=d-2
        System.out.println(d);
        
        System.out.println("Conditional operators: ");
        int result=(a==b) ? 1:2;
        System.out.println("resultado: "+result);
        a=5;
        b=4;
        result=(--a==b) ? 1:2;
        System.out.println(result);
        
        
        
    }
    }
        
    

