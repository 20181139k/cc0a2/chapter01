/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davisalderetev.p22043001;

/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class Main {
    
    public static void main(String[] args){
        
        System.out.println("Loop statements");
        
        System.out.println("For Loop");
        
        for(int i=0;i<10;i++){
            System.out.println(i);
        }
        
        for(int i=9;0<=0;i--){
            System.out.println(i);
        }
        
        //Matrix 5x3
        

       for(int i=0;i<=5;i++){
           // System.out.println("i: "+i);
            for(int j=0;j<=3;j++){
                System.out.println("("+i+","+j+")"+'\t');
            }
            System.out.println("");
        }
       
        System.out.println("While loop");
        int i=0;
        while(i<10){
            i++;
            System.out.println(i);
        }
        i=10;
        while(0<i){
            System.out.println(i);
            i--;
        }
        i=1;
        int f=5;
        int c=3;
        while(i<=f){
            
            int j=1;
            while(j<=c){
                
               System.out.println("("+i+","+j+")"+'\t');
               j++;
            }
            System.out.println("");
            i++;
        }
        i=0;
        while(i<0){
            System.out.println("message");
        }
        do{
            System.out.println("This do while message is printed");
        }while(i<0);
        
        i=1;
        do{
            int j=1;
            do{
                System.out.println("("+i+","+j+")"+'\t');
                j++;
            }while(j<=c);
            System.out.println("");
            i++;
        }while(i<=f);
        //print the first numbers between 10 and 90
        
        int p=10;
        for(int p=10;p<=90;p++){
            boolean t=true;
            for(int j=2;j<p;j++){
            if(p%j==0){
                //System.out.println("This number is not prime");
                t=false;
                break;
            }
        }
            if(p==53) continue;
            if(t){
            System.out.println("The number is prime: "+p);
        }
            
        }
        
    }
}
