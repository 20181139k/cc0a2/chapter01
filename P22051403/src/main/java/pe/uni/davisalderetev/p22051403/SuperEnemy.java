/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davisalderetev.p22051403;

/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class SuperEnemy extends Enemy implements Shield{
    private String name;
    private int shield;
    
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public int getShield(){
        return shield;
    }
    public void setShield(int shield){
        this.shield=shield;
    }
    public SuperEnemy(){
        this.name="Predator";
        this.shield=1;
    }
    public SuperEnemy(String name){
        super(100,1000);
        this.name=name;
        this.shield=1;
    }
    @Override
    public void bonusLife(){
        super.addLife(50);
    }
     public String toString(){
        return "Enemy["+"name: "+name+", life: "+super.getLife()+", health: "+super.getHealth()+", Shield: "+shield+")";
    }
     
    @Override
     public void bonusShield(){
         shield+=5;
     }
}
