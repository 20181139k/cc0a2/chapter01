/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davisalderetev.p22042803;

/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Conditional Statements");
        /*
        int i=35;
        if(30<i){
            System.out.println("The number is greater to 30");
        }
        if(i%2==1){
            System.out.println("The number is odd");
        }
        if(i%5==0){
            System.out.println("The number is multiple of 5");
        }
        -------------------------------------------------------------------
        -------------------------------------------------------------------------
        
        int i=29;
        if(30<i){
            System.out.println("The number is greater to 30");
        }else
        if(i%2==1){
            System.out.println("The number is odd");
        }else
        if(i%5==0){
            System.out.println("The number is multiple of 5");
        }else{
            System.out.println("the number is other");
        }
        */
        
        int i=35;
        if(30<i){
            System.out.println("The number is greater to 30, and the programs end");
            return;
        }
        if(i%2==1){
            System.out.println("The number is odd, and the program ends");
            return;
        }
        if(i%5==0){
            System.out.println("The number is multiple of 5, and the programs end");
            return;
        }
        System.out.println("here we can follow this program");
    }
}
