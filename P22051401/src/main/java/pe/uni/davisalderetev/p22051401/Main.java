/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davisalderetev.p22051401;

/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class Main {
    
    public static void main(String[] args){
        System.out.println("Exceptions ...");
        int[] array={10,20,30,40,50,60};
        try{
            int i=8;
           
                System.out.println(array[i]);
                int j=i/0;
                System.out.println("End of try...");
        } catch(ArithmeticException e){
            System.out.println("/ by Zero");
            System.out.println(e);
        } catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Array index error");
            System.out.println(e);
        } catch(Exception e){
        System.out.println(e);
    }finally{
            System.out.println("CLose DB connection, even if the query is not completed");
        }
        System.out.println("End of aplication...");
    }
    
   public static void Function () throws ArithmeticException{
        int j=1/0; 
        throw new ArithmeticException();
    }
    
}
