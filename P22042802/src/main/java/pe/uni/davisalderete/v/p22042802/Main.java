/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davisalderete.v.p22042802;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Input console");
        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
        try {
            String name=reader.readLine();
            System.out.println("hola "+ name+" !!!!");
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
