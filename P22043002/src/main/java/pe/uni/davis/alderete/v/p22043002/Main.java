/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davis.alderete.v.p22043002;

import static java.lang.Math.*;
//import static java.lang.Math.round;

/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Math functions");
        float i=10.25f;
        //
        System.out.println("i: "+round(i));
        //log
        System.out.println(E);
        System.out.println("ln(e): "+ log(E));
        //power
        System.out.println("2^8: "+pow(2,8));
        System.out.println("256^(1/2): "+pow(256,0.5));
        //sqrt
        System.out.println("256^(1/2)"+sqrt(256));
        
        //trigonometric funtcions
        System.out.println("sen(0): "+sin(0));
        System.out.println("sin(3.14/2): "+sin(PI/2));
        
        
    }
}
