/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davisalderetev.p22051402;

/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Exceptio 2...");
        try{
            myExceptionFunction();
            Function();
            
        }catch(ArithmeticException e){
            System.out.println("Arithmetic exception: "+e);
        }catch(MyException e){
            System.out.println("My exception: "+e);
        }
        catch(Exception e){
            System.out.println("End..."+e);
        }
    }
    public static void Function() throws ArithmeticException{
        int j=1/0;
    }
    public static void myExceptionFunction() throws MyException{
        throw new MyException("My error !!!");
    }
}
