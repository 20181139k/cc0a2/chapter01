/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.davisalderetev.p22050501;

import java.util.Arrays;

/**
 *
 * @author Davis Alderete <davis.alderete.v@uni.pe>
 */
public class Main {
    public static void main(String[] args){
        System.out.println("Strings");
        String string="Hellos world !!!";
        System.out.println(string);
        System.out.println("Lenght of string: "+string.length());
        System.out.println("Character's location");
        System.out.println("1st character: "+string.charAt(0));
        System.out.println("Last character: "+string.charAt(string.length()-1));
        System.out.println("Convertions");
        System.out.println(string.toUpperCase());
        System.out.println("Splits");
        String[] parts=string.split(" ");
        System.out.println("Part's size: "+parts.length);
        for(int i=0;i<parts.length;i++){
            System.out.println(parts[i]);
        }
        System.out.println("parts: "+Arrays.toString(parts));
        System.out.println("trim");
        String trim=string.trim();
        System.out.println("Length of trims: "+trim.length());
        System.out.println(trim);
    }
}
